.PHONY: strip

GCC ?= x86_64-w64-mingw32-gcc
convert.exe: convert.c
	$(GCC) -Os $(?) -static -o $(@)
strip: convert.exe
	strip $(?)
