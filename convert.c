#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>

#if __BYTE_ORDER__ != __ORDER_LITTLE_ENDIAN__
#error "Too lazy to make it portable"
#endif

const int8_t header[] = {'E', 'R', 'T', 'M'};
// Texture file version where
// we need to replace NIBM header with ERTM
#define VERSION_OLD 0x077330F3
#define VERSION_OFFSET 0x10

#define die(x, msg) \
	if ((x)) { \
		perror("IO Error: "msg); \
		return 1; \
	}

int main(int argc, char* args[]) {
	int32_t version_in_file;
	if (argc != 2) {
		return 1;
	}
	printf("Fixing file: %s\n", args[1]);
	FILE* file = fopen(args[1], "r+");
	die(!file, "opening")
	// go to version offset

	die(fseek(file, VERSION_OFFSET, SEEK_SET), "version seek");
	if(fread(&version_in_file, 4, 1, file) != 1) {
		printf("Too short file for version read\n");
		return 1;
	} 
	if (version_in_file != VERSION_OLD) {
		printf("Invalid version. Expected %d, got %d Aborting\n",
			VERSION_OLD, version_in_file);
		return 1;
	}
	// rewind to start
	rewind(file);
	die(fwrite(&header, 1, 4, file) != 4, "write");
	die(fflush(file), "flush");
	die(fclose(file), "close");
	printf("Header change done\n");
	return 0;
}
